## Welcome to a World of Fractals

A simple web project that creates an animated Binary Fractal Tree, and renders a Barnsley Fern that the user can interact with.

## Access

The project can be viewed via the following [url](http://mathproject.gitlab.io/). Try using Chrome if you have difficulty viewing the fractals.

## Citation

To better understand how fractals may be drawn, the following sources were examined for inspiration and code snippets.

[Barnsley Fern Fractal](https://stackoverflow.com/questions/15536849/javascript-barnsley-fern-fractal) (Code Inspiration)

[Binary Tree Fractal](http://jsfiddle.net/77trey/5h2uyu78/) (Code Inspiration)

The background images used for the parallax effect are listed below. No copyright infringement intended.

[Starscape Background](https://abluescarab.deviantart.com/art/Starscape-2-185535979) (Image)

[Blue Gradient Background](http://pcwallart.com/light-blue-background-wallpaper-1.html) (Image)

## License

The MIT License (MIT) - Copyright (C) 2017 K. K.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.