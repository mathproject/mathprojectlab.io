/* **********************************
   Part A: Generating a Barnsley Fern
   ********************************** */

var sliderIter = document.getElementById("iter");
var sliderLow = document.getElementById("low");
var sliderMid = document.getElementById("mid");
var sliderHigh = document.getElementById("high");

var numIter = document.getElementById("iter-num");
var numLow = document.getElementById("low-num");
var numMid = document.getElementById("mid-num");
var numHigh = document.getElementById("high-num");

var myCanvasF = document.getElementById("myCanvasF");
var ctxF = myCanvasF.getContext("2d");

var wrapperDivF = document.getElementById("wrapperF");

myCanvasF.width = wrapperDivF.clientWidth - 24;
myCanvasF.height = wrapperDivF.clientHeight;

var color = "#233f00";

ctxF.fillStyle = color;
ctxF.translate(ctxF.canvas.width / 2, ctxF.canvas.height / 2);

var x = 0;
var y = 0;

var low = 1;
var mid = 86;
var high = 94;

var iterations = 30000;

// Wrapper function to get back a random number
var generateRandomNumber = function(min, max){
    return (Math.random() * max) + min;
}

// Key section of the code used to generate the fractal
var generateFern = function () {        
    for (var i = 0; i < iterations; i++) {
        var rand = generateRandomNumber(0,100);
        if (rand < low) {
            one();
        } else if (rand < mid) {
            two();
        } else if (rand < high) {
            three();
        } else {
            four();
        }
    }
}

// First Barnsley Contraction
var one = function () {
    var xn = x;
    var yn = y;
    var zx = 0;
    var zy = 0.16 * yn;
    drawPoint(zx, zy);
}

// Second Barnsley Contraction
var two = function () {
    var xn = x;
    var yn = y;
    var zx = 0.85 * xn + 0.04 * yn;
    var zy = -0.04 * xn + 0.85 * yn + 1.6;
    drawPoint(zx, zy);
}

// Third Barnsley Contraction
var three = function () {
    var xn = x;
    var yn = y;
    var zx = 0.2 * xn - 0.26 * yn;
    var zy = 0.23 * xn + 0.22 * yn + 1.6;
    drawPoint(zx, zy);
}

// Fourth Barnsley Contraction
var four = function () {
    var xn = x;
    var yn = y;
    var zx = -0.15 * xn + 0.28 * yn;
    var zy = 0.26 * xn + 0.24 * yn + 0.44;
    drawPoint(zx, zy);
}

// Code that will actually draw the point
var drawPoint = function (zx, zy) {
    x = zx;
    y = zy;
    var cx = (+zx*(myCanvasF.width/30.975));
    var cy = (-zy*(myCanvasF.height/12.675)) + myCanvasF.height/2.5;
    ctxF.fillRect(cx, cy, 1, 1);
}

// Invoked when any of the sliders are adjusted
var redrawCanvas = function() {
    x = y = 0;
    myCanvasF.width = myCanvasF.width;
    ctxF.fillStyle = color;
    ctxF.translate(ctxF.canvas.width / 2, ctxF.canvas.height / 2);
    generateFern();
}

// Iteration Count Slider
sliderIter.oninput = function() {
    iterations = this.value;
    numIter.innerHTML = this.value;
    redrawCanvas();
}

// Low Limit (Probability) Slider
sliderLow.oninput = function() {
    low = this.value;
    numLow.innerHTML = this.value;
    redrawCanvas();
}

// Middle Limit (Probability) Slider
sliderMid.oninput = function() {
    mid = this.value;
    numMid.innerHTML = this.value;
    redrawCanvas();
}

// High Limit (Probability) Slider
sliderHigh.oninput = function() {
    high = this.value;
    numHigh.innerHTML = this.value;
    redrawCanvas();
}

// Button OnClick Event Handler
var resetValues = function() {
    low = numLow.innerHTML = sliderLow.value = 1;
    mid = numMid.innerHTML = sliderMid.value = 86;
    high = numHigh.innerHTML = sliderHigh.value = 94;
    iterations = numIter.innerHTML = sliderIter.value = 30000;
    
    redrawCanvas();
}   

generateFern();

/* ****************************************
   Part B: Generating a Binary Fractal Tree
   **************************************** */

var myCanvasT = document.getElementById("myCanvasT");
var ctxT = myCanvasT.getContext('2d');

var wrapperDivT = document.getElementById("wrapperT");

myCanvasT.width = wrapperDivT.clientWidth - 24;
myCanvasT.height = wrapperDivT.clientHeight;

ctxT.fillStyle = "#000000";
ctxT.translate (ctxT.canvas.width/2, ctxT.canvas.height/2);

var angle = Math.PI / 2;
var iterationsT = 1;

var colors = ["#98b700", "#6db700", "#00b73d", "#00b7ad", "#009bb7"];

var colorId = 0;

var leftBranchAngle = Math.PI / 8;
var rightBranchAngle = Math.PI / 4;

var iterate = function (startingPoint, deg, iteration){
    var length = (iterationsT / iteration) * 13;
    var lineBeginning = startingPoint;
    var size = length/15;

    var lineEnding = new point(lineBeginning.x + length * Math.cos(deg + leftBranchAngle), lineBeginning.y + length * Math.sin(deg + leftBranchAngle));
    var lineEnding2 = new point(lineBeginning.x + length * Math.cos(deg - rightBranchAngle), lineBeginning.y + length * Math.sin(deg - rightBranchAngle));

    drawLine(lineBeginning, lineEnding, size);
    drawLine(lineBeginning, lineEnding2, size);

    if(iteration > iterationsT){
        ctxT.fillStyle = colors[colorId % colors.length];
        colorId++;
        ctxT.arc(lineEnding.x, -lineEnding.y, 5, 0, 2 * Math.PI, false);
        ctxT.closePath();
        ctxT.arc(lineEnding2.x, -lineEnding2.y, 3, 0, 2 * Math.PI, false);
        ctxT.fill();
        return;
    }

    iterate(lineEnding, deg + leftBranchAngle, iteration + 1);
    iterate(lineEnding2, deg - rightBranchAngle, iteration + 1);
}

var generateTree = function(){
    if(iterationsT < 9){
      requestAnimationFrame(generateTree);
      iterationsT += 0.05;
    }
    
    clear();
    colorId = 0;

    drawLine(new point(0,-100), new point(0, -200), 10);
    iterate(new point(0,-100), angle, 1);
}

requestAnimationFrame(generateTree);

var clear = function(){
    ctxT.clearRect(-myCanvasT.width/2, -myCanvasT.height/2, myCanvasT.width, myCanvasT.height);
}

var point = function(x, y){
    this.x = x;
    this.y = y;
}

var drawLine = function(pointA, pointB, size){
    x1 = pointA.x;
    y1 = -pointA.y;
    x2 = pointB.x;
    y2 = -pointB.y;

    ctxT.strokeStyle = "rgba(255, 255, 255," + (size/iterationsT + 0.3) + ")";

    ctxT.lineWidth = size;
    ctxT.beginPath();
    ctxT.moveTo(x1, y1);
    ctxT.lineTo(x2, y2);
    ctxT.stroke();
    ctxT.closePath();
}

var resizeCanvas = function() {
    // Code corresponding to the Barnsley Fern
    myCanvasF.width = wrapperDivF.clientWidth - 24;
    myCanvasF.height = wrapperDivF.clientHeight;
    
    x = y = 0;
    
    ctxF.fillStyle = color;
    ctxF.translate(ctxF.canvas.width / 2, ctxF.canvas.height / 2);
    
    generateFern();
    
    // Code corresponding to the Binary Fractal Tree
    myCanvasT.width = wrapperDivT.clientWidth - 24;
    myCanvasT.height = wrapperDivT.clientHeight;
    
    ctxT.fillStyle = "#000000";
    ctxT.translate (ctxT.canvas.width/2, ctxT.canvas.height/2);
    
    requestAnimationFrame(generateTree);
}

// Resize the canvas to fill the parent window dynamically
window.addEventListener('resize', resizeCanvas, false);
